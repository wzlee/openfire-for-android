package com.hzaccp.openfiretest.util;

import java.util.LinkedList;
import java.util.List;

import com.hzaccp.openfiretest.biz.SE;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;

/**
 * 退出工具类
 * */
public class ExitUtil extends Application {
	private List<Activity> activityList = new LinkedList<Activity>();
	private static ExitUtil instance;

	private ExitUtil() {
	}

	/**
	 * 单例模式中获取唯一的ExitUtil实例
	 * */
	public static ExitUtil getInstance() {
		if (null == instance) {
			instance = new ExitUtil();
		}
		return instance;
	}

	/**
	 * 添加Activity到容器中
	 * */
	public void addActivity(Activity activity) {
		activityList.add(activity);
	}

	/**
	 * 遍历所有Activity并finish
	 * */
	public void exit(Context context) {
		new AlertDialog.Builder(context).setTitle("确认退出吗？").setIcon(android.R.drawable.ic_dialog_info)
				.setPositiveButton("确定", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						for (Activity activity : activityList) {
							activity.finish();
						}
						SE.getInstance().logOut();//退出登录
						System.exit(0);
					}
				}).setNegativeButton("返回", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
					}
				}).show();
	}
}
